const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TierAssetSchema = new Schema({
  name: String,
  description: String,
  price: Number
});

module.exports = mongoose.model("TierAsset", TierAssetSchema);
