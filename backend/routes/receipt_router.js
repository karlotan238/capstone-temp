const express = require("express");

const ReceiptRouter = express.Router();

const ReceiptModel = require("../models/Receipt");


ReceiptRouter.post("/addreceipt", async (req, res) => {
  try {
    let receipt = ReceiptModel({
      name: req.body.name,
      description: req.body.description,
      price: req.body.price
    });

    receipt = await receipt.save();

    res.send(receipt);
  } catch (e) {
    console.log(e);
  }
});

ReceiptRouter.get("/showreceipts", async (req, res) => {
  try {
    let receipts = await ReceiptModel.find();
    res.send(receipts);
  } catch (e) {
    console.log(e);
  }
});

ReceiptRouter.get('/showreceiptbyid/:id' async (req, res)=>{
  try {
    let receipt = await ReceiptModel.findById(req.params.id);
    res.send(receipt);
  } catch (e) {
    console.log(e)
  }
})

ReceiptRouter.delete('/deletereceipt/:id', async(req, res)=>{
	try{
		let deletedReceipt = await ReceiptModel.findByIdAndDelete(req.params.id);
		res.send(deletedReceipt);
	}catch(e){
		console.log(e)
	}
});

module.exports = ReceiptRouter;