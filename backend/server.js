const express = require("express");
const app = express();
const mongoose = require("mongoose");
const config = require("./config");
const cors = require("cors");

mongoose
  .connect(
    "mongodb+srv://adminkarlo:adminpw@cluster0-gvyac.mongodb.net/test?retryWrites=true&w=majority",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    }
  )
  .then(() => {
    console.log("Remote Database Connection Established");
  });

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());

//assets API

app.listen(config.port, () => {
  console.log(`Listening on Port ${config.port}`);
});

const users = require("./routes/users_router");
app.use("/", users);

const tierAssets = require("./routes/tierasset_router");
app.use("/", users);
